const app = getApp();
const host = 'https://www.qzyxs.com/api';
const hostUrl = 'https://www.qzyxs.com';
let num = 0
let header = {
  "content-type": "application/json;charset=UTF-8"
}
const formatParams = (data) => {
  let arr = [];
  for (let name in data) {
    arr.push(encodeURIComponent(name) + "=" + encodeURIComponent(data[name]));
  }
  return arr.join("&");
}

function httpService(url, doData, doMethod, doSuccess, doFail, author) {
  if (author) {
    header = {
      "content-type": "application/json;charset=UTF-8"
    }
  } else {
    header = wx.getStorageSync('header')
  }
  if (doMethod == 'post') {
    wx.request({
      url: host + url,
      data: doData,
      method: doMethod,
      header: header,
      timeout: 15000,
      success(res) {
        if (res.header['Set-Cookie']) {
          header = {
            "content-type": "application/json;charset=UTF-8",
            "cookie": res.header['Set-Cookie'].split(';')[0]
          }

          wx.setStorage({
            key: "header",
            data: header
          })
        }
        if (num === 4) {
          setTimeout(function () {
            num = 0
          }, 4000)
        }
        if (res.statusCode === 401 && num < 4) {
          auto(url, doData, doMethod, doSuccess, doFail, author)
        } else if(res.data.code == -2|| res.data.code == 1001|| res.data.code == 1003){
          console.log(123321)
          wx.navigateTo({
            url: '/pages/login/login',
          }) 
        }else{
          doSuccess(res.data)
        }
      },
      fail(res) {
        doFail(res)
        wx.showToast({
          title: '网络连接失败,请检查网络',
          icon: 'none',
          duration: 2000
        })
      }
    })
  } else {
    wx.request({
      url: host + url + '?' + formatParams(doData),
      method: doMethod,
      header: header,
      timeout: 15000,
      success(res) {
        if (res.header['Set-Cookie']) {
          header = {
            "content-type": "application/json;charset=UTF-8",
            "cookie": res.header['Set-Cookie'].split(';')[0]
          }
          wx.setStorage({
            key: "header",
            data: header
          })
        }
        if (num === 4) {
          setTimeout(function () {
            num = 0
          }, 4000)
        }
        if (res.statusCode === 401 && num < 4) {
          auto(url, doData, doMethod, doSuccess, doFail, author)
        } else if(res.data.code == -2|| res.data.code == 1001|| res.data.code == 1003){
          console.log(123321)
          wx.navigateTo({
            url: '/pages/login/login',
          }) 
        }else {
          doSuccess(res.data)
        }
      },
      fail(res) {
        console.log(res)
        if(res.data.code == -2|| res.data.code ==1001){
          console.log(123321)
          wx.navigateTo({
            url: '/pages/login/login',
          }) 
        }
        doFail(res)
        console.log(res)
        wx.showToast({
          title: '网络连接失败,请检查网络',
          icon: 'none',
          duration: 2000
        })
      }
    })
  }
}

function auto(url, doData, doMethod, doSuccess, doFail, author) {
  wx.login({
    success: ele => {
      httpService('auth/' + ele.code, '', 'get', (res) => {
        setTimeout(function () {
          httpService(url, doData, doMethod, doSuccess, doFail, author)
          num += 1
        }, 1000)
      }, (err) => {}, true)
    }
  })
}
module.exports = {
  httpService: httpService,
  host: host,
  hostUrl:hostUrl
}