//app.js
const call = require('./utils/request.js')
App({
  onLaunch: function () {
    let that = this
    wx.getSystemInfo({
      success: function (res) {
        that.globalData.statusBarHeight = res.statusBarHeight
      }
    })
  },
  onShow(options) {
    if (options.path == "pages/login/null") {

    }
  },
  onHide() {
  },
  globalData: {
    pageShow: false,
    statusBarHeight: 0,
    userInfo: null,
    global_bac_audio_manager: {
      is_play: false,
      id: '',
      play_time: '',
      article_id: '',
    }
  }
})