// pages/proList/proList.js
const call = require("../../utils/request.js");
Page({

    /**
     * 页面的初始数据
     */
    data: {
        scrollHeight: 0,
        dataList: [],
        perType: wx.getStorageSync('LOGINDATA').perType,
        page: 1,
        total: 0,
        ctcProgress: '',
        type: ''
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        let windowHeight = wx.getSystemInfoSync().windowHeight // 屏幕的高度
        this.setData({
            scrollHeight: windowHeight,
            ctcProgress: options.ctcProgress,
            type: options.type
        })
        this.getData()
    },
    getData() {
        // 获取页面初始数据
        let that = this
        let data = {
            ctcProgress: that.data.ctcProgress,
            page: that.data.page,
            limit: 10
        }
        call.httpService("/ctclist", data, "post", (res) => {
                if (res.code === 0) {
                    if (that.data.perType == 1 && that.data.type == 3 && (res.data.length === 1)) {
                        wx.navigateTo({
                            url: '/pages/reportList/reportList?type=3&projectId=' + res.data[0].ctcid
                        })
                    }
                    
                    res.data.map((item)=> {
                        item.type = 0
                        let dateType =  ''
                        if(item.startTime){
                            dateType = new Date().getTime() - that.dateToTimestamp(item.startTime)
                            item.type = item.startTime&&dateType?1:0
                        }
                        
                    })
                    that.setData({
                        dataList: that.data.page == 1 ? res.data : that.data.dataList.concat(res.data),
                        total: res.count
                    });


                } else {
                    wx.showToast({
                        title: res.msg,
                        icon: "none",
                    });
                }
                wx.hideLoading();
            },
            (err) => {
                console.log(err)
                wx.hideLoading();
            }
        );
    },
    dateToTimestamp(date) {
        const dateArray = date.split('-')
        const year = parseInt(dateArray[0])
        const month = parseInt(dateArray[1])
        const day = parseInt(dateArray[2])
        return Date.parse(year, month - 1, day, 0, 0, 0, 0)
    },
    toUrl(e) {
        // 跳转日报列表
        if (this.data.type == 1) {

        } else if (this.data.type == 2) {
            wx.navigateTo({
                url: '/pages/doingList/doingList?projectId=' + e.currentTarget.dataset.id
            })
        } else if (this.data.type == 3) {

        } else if (this.data.type == 4) {
            wx.navigateTo({
                url: '/pages/proDetail/proDetail?projectId=' + e.currentTarget.dataset.id
            })
        }
        wx.navigateTo({
            url: '/pages/reportList/reportList?projectId=' + e.currentTarget.dataset.id + '&type=' + this.data.type
        })
    },
    scrolltolower() {
        if (this.data.total > this.data.dataList.length) {
            this.setData({
                page: this.data.page + 1
            })
            this.getData()
        }
    },
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})