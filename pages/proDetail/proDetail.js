// pages/proDetail/proDetail.js
const call = require("../../utils/request.js");
Page({

  /**
   * 页面的初始数据
   */
  data: {
    scrollHeight: 0,
    dataList: {},
    projectId: '',
    flowData:{}
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let windowHeight = wx.getSystemInfoSync().windowHeight // 屏幕的高度
    this.setData({
      scrollHeight: windowHeight,
      projectId: options.projectId

    })
    this.getData()
  },
  getData() {
    // 获取页面初始数据
    let that = this
    let data = {
      ctcId: that.data.projectId
    }
    call.httpService("/ctcbyid", data, "post", (res) => {
        if (res.code === 0) {
          that.setData({
            dataList: res.data
          })
          wx.setNavigationBarTitle({
            title:  res.data.ctcName
        })
          if(res.data.flowlist&& res.data.flowlist.phaselist&&res.data.flowlist.phaselist.length>0){
            that.setData({
              flowData: res.data.flowlist.phaselist
            })
          }
          console.log(that.data.dataList)
        } else {
          wx.showToast({
            title: res.msg,
            icon: "none",
          });
        }
        wx.hideLoading();
      },
      (err) => {
        console.log(err)
        wx.hideLoading();
      }
    );
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})