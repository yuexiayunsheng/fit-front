// pages/my/my.js
const call = require("../../utils/request.js");

Page({
  data: {
    funArr: [{
        name: "今日日报",
        src: "/assets/fix/dayReport.png",
        url: "/pages/proList/proList?ctcProgress=0&type=3",
        id: "a0",
      },{
        name: "历史日报",
        src: "/assets/fix/hisDayreport.png",
        url: "/pages/proList/proList?ctcProgress=0&type=1",
        id: "a1",
      },{
        name: "项目进度",
        src: "/assets/fix/doing.png",
        url: "/pages/proList/proList?ctcProgress=0&type=2",
        id: "a2",
      },
      {
        name: "历史项目",
        src: "/assets/fix/done.png",
        url: "/pages/historyList/historyList?ctcProgress=1",
        id: "a3",
      },
      {
        name: "消息",
        src: "/assets/fix/notice.png",
        url: "/pages/notice/notice",
        id: "a4",
      },
      {
        name: "问题反馈",
        src: "/assets/fix/reBack.png",
        url: "/pages/notice/reBack",
        id: "a4",
      }
    ],
    proArr: [{
      name: "项目资料",
      src: "/assets/fix/projectInfo.png",
      url: "/pages/proList/proList?ctcProgress=-1&type=4",
      id: "a5",
    }],
    startTime: '',
    endTime: '',
    loginData: {},
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function (options) {
    let that = this;
    //console.log(wx.getStorageSync('LOGINDATA'))
    if (wx.getStorageSync('LOGINDATA')) {
      that.setData({
        loginData: {
          perName: wx.getStorageSync('LOGINDATA').perName,
          userId: wx.getStorageSync('LOGINDATA').userId,
          perAccount: wx.getStorageSync('LOGINDATA').perAccount,
          perType: wx.getStorageSync('LOGINDATA').perType
        }
      })
    } else {
      wx.navigateTo({
        url: '/pages/login/login',
      })
    }
    if (that.data.loginData.perType == 1) {
      that.setData({
        proArr: [],
        funArr: [{
            name: "今日日报",
            src: "/assets/fix/dayReport.png",
            url: "/pages/proList/proList?ctcProgress=0&type=3",
            id: "a0",
          },{
            name: "历史日报",
            src: "/assets/fix/hisDayreport.png",
            url: "/pages/proList/proList?ctcProgress=0&type=1",
            id: "a1",
          },
          {
            name: "历史项目",
            src: "/assets/fix/done.png",
            url: "/pages/historyList/historyList?ctcProgress=1",
            id: "a0",
          }
        ],
      })
    } else if (that.data.loginData.perType == 3) {
      that.setData({
        funArr: [{
            name: "今日日报",
            src: "/assets/fix/dayReport.png",
            url: "/pages/proList/proList?ctcProgress=0&type=3",
            id: "a0",
          },{
            name: "历史日报",
            src: "/assets/fix/hisDayreport.png",
            url: "/pages/proList/proList?ctcProgress=0&type=1",
            id: "a1",
          }, {
            name: "项目进度",
            src: "/assets/fix/doing.png",
            url: "/pages/proList/proList?ctcProgress=0&type=2",
            id: "a2",
          },
          {
            name: "历史项目",
            src: "/assets/fix/done.png",
            url: "/pages/historyList/historyList?ctcProgress=1",
            id: "a3",
          }
        ],
      })
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  skip(e) {
    // console.log(e.currentTarget.dataset.url)
    wx.navigateTo({
      url: e.currentTarget.dataset.url
    });
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {},

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function () {
   
  },

  setting() {
    wx.navigateTo({
      url: '/pages/setting/setting',
    })
  },
  // 分享
  onShareAppMessage: function (res) {},
});