// pages/historyList/historyList.js
const call = require("../../utils/request.js");
Page({

  /**
   * 页面的初始数据
   */
  data: {
    scrollHeight: 0,
    dataList: [],
    page: 1,
    total: 0,
    perType:wx.getStorageInfoSync('LOGINDATA'),
    ctcProgress: 1
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let windowHeight = wx.getSystemInfoSync().windowHeight // 屏幕的高度
    this.setData({
      scrollHeight: windowHeight
    })
    this.setData({
      ctcProgress: options.ctcProgress
    })
    this.getData()
  },
  getData() {
    // 获取页面初始数据
    let that = this
    let data = {
      ctcProgress: 1,
      page: that.data.page,
      limit: 10
    }
    call.httpService("/ctclist", data, "post", (res) => {
        if (res.code === 0) {
          that.setData({
            dataList: that.data.page == 1 ? (res.data||[]) : that.data.dataList.concat(res.data),
            total: res.count
          });
        } else {
          wx.showToast({
            title: res.msg,
            icon: "none",
          });
        }
        wx.hideLoading();
      },
      (err) => {
        console.log(err)
        wx.hideLoading();
      }
    );
  },
  toUrl(e) {
    console.log(e.currentTarget.dataset.id)
    wx.navigateTo({
      url: '/pages/reportList/reportList?projectId=' + e.currentTarget.dataset.id
    })
    
  },
  scrolltolower() {
    if (this.data.total > this.data.dataList.length) {
      this.setData({
        page: this.data.page + 1
      })
      this.getData()
    }else{
      wx.showToast({
        title:'暂无更多数据',
        icon: "none",
      });
    }
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})