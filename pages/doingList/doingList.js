// pages/doingList/doingList.js
const call = require("../../utils/request.js");
Page({

  /**
   * 页面的初始数据
   */
  data: {
    scrollHeight:0,
    projectId:'',
    steps: [],
    perType:wx.getStorageSync('LOGINDATA').perType
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let windowHeight = wx.getSystemInfoSync().windowHeight // 屏幕的高度
    this.setData({
      scrollHeight: windowHeight,
      projectId:options.projectId
    })
    console.log(this.data.perType)
    this.getData()
  },
  makePhone(e){
    wx.makePhoneCall({
      phoneNumber: e.currentTarget.dataset.phone
    })
  },
  getData() {
    // 获取页面初始数据
    let that = this
    let data = {
      ctcId: that.data.projectId
    }
    call.httpService("/relflow/phaselist", data, "post", (res) => {
        if (res.code === 0) {
          that.setData({
            steps:res.data
          })
        } else {
          wx.showToast({
            title: res.msg,
            icon: "none",
          });
        }
        wx.hideLoading();
      },
      (err) => {
        console.log(err)
        wx.hideLoading();
      }
    );
  },
  apply(e){
    // 申请
    let that = this
    let data = {
      phaseId: e.currentTarget.dataset.id
    }
    call.httpService("/relflow/check", data, "post", (res) => {
        if (res.code === 0) {
          wx.showToast({
            title: '已完成申请，等待业主审批',
            icon: "none",
          });
          that.getData()
        } else {
          wx.showToast({
            title: res.msg,
            icon: "none",
          });
        }
        wx.hideLoading();
      },
      (err) => {
        console.log(err)
        wx.hideLoading();
      }
    );
  },
  takePhone(e){
    // 拨打电话
    wx.makePhoneCall({
      phoneNumber: e.target.dataset.phone
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})