// pages/login/login.js
const call = require("../../utils/request.js");
Page({

  /**
   * 页面的初始数据
   */
  data: {
    username: '',
    password: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },
  // 获取输入账号 
  usernameInput: function (e) {
    this.setData({
      username: e.detail.value
    })
  },

  // 获取输入密码 
  passwordInput: function (e) {
    this.setData({
      password: e.detail.value
    })
  },
  // 登录处理
  login: function () {
    let that = this;
    let reg_tel = /^(13[0-9]|14[01456879]|15[0-35-9]|16[2567]|17[0-8]|18[0-9]|19[0-35-9])\d{8}$/;
    // if (!reg_tel.test(this.data.username)) {
    //   wx.showToast({
    //     title: '请输入正确的手机号',
    //     icon: 'none',
    //     duration: 2000
    //   })
    //   return false
    // }

    if (this.data.password.length == 0) {
      wx.showToast({
        title: '请输入密码',
        icon: 'none',
        duration: 2000
      })
      return false
    }
    let data = {
      perAccount: that.data.username,
      perPasswd: that.data.password
    }
    call.httpService("/user/login",data,"post",(res) => {
        if (res.code === 0) {
          wx.setStorageSync('LOGINDATA', res.data);
          wx.switchTab({
            url: '/pages/my/my'
          })
        } else {
          wx.showToast({
            title: res.msg,
            icon: "none",
          });
        }
        wx.hideLoading();
      },
      (err) => {
        wx.hideLoading();
      },false
    )
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})