// pages/setting/setting.js
const call = require("../../utils/request.js");
Page({

  /**
   * 页面的初始数据
   */
  data: {
    editShow: false,
    loginoutShow: false,
    pwdShow: false,
    oldPwd: '',
    newPwd: '',
    repeatPwd: '',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },
  editPwd() {
    this.setData({
      editShow: true,
      pwdShow: true
    })
  },
  savePwd() {
    // 保存修改密码
    if (!this.data.newPwd || !this.data.repeatPwd || this.data.newPwd.length < 6 || this.data.repeatPwd.length < 6) {
      wx.showToast({
        title: '请正确输入密码',
        icon: 'none',
        duration: 2000
      })
      return false
    }
    if (this.data.newPwd != this.data.repeatPwd) {
      wx.showToast({
        title: '两次输入密码不一致',
        icon: 'none',
        duration: 2000
      })
      return false
    }
    let data = {
      userId: wx.getStorageSync('LOGINDATA').userId,
      perPasswd: this.data.newPwd
    }
    call.httpService("/user/update", data, "post", (res) => {
        if (res.code === 0) {
          wx.clearStorageSync('LOGINDATA');
          wx.showToast({
            title: '修改成功，请重新登录',
            icon: 'none',
            duration: 2000
          })
          setTimeout(()=>{
            wx.navigateTo({
              url: '/pages/login/login'
            })
          },2000)
         
        } else {
          wx.showToast({
            title: res.msg,
            icon: "none",
          });
        }
        wx.hideLoading();
      },
      (err) => {
        wx.hideLoading();
      }, false
    )
    this.setData({
      editShow: false,
      pwdShow: false
    })
  },
  canclePwd() {
    this.setData({
      editShow: false,
      pwdShow: false
    })
  },
  // // 获取旧密码
  // oldInput: function (e) {
  //   this.setData({
  //     oldPwd: e.detail.value
  //   })
  // },
  // 获取新密码
  newInput: function (e) {
    this.setData({
      newPwd: e.detail.value
    })
  },
  // 获取重复密码
  repeatInput: function (e) {
    this.setData({
      repeatPwd: e.detail.value
    })
  },
  loginOut() {
    this.setData({
      editShow: true,
      loginoutShow: true
    })
  },
  cancleLogin() {
    this.setData({
      editShow: false,
      loginoutShow: false
    })
  },
  loginOutSave() {
    let data = {
      perAccount: wx.getStorageSync('LOGINDATA').perAccount,
    }
    call.httpService("/user/exit", data, "post", (res) => {
        if (res.code === 0) {
          wx.clearStorageSync();
          wx.showToast({
            title: '已退出，2秒后跳转首页',
            icon: 'none',
            duration: 2000
          })
          setTimeout(()=>{
            wx.switchTab({
              url: '/pages/home/index'
            })
          },2000)
          
        } else {
          wx.showToast({
            title: res.msg,
            icon: "none",
          });
        }
        wx.hideLoading();
      },
      (err) => {
        wx.hideLoading();
      }, false
    )
    this.setData({
      editShow: false,
      loginoutShow: false
    })
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})