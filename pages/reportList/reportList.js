// pages/proList/proList.js
const call = require("../../utils/request.js");
const util = require("../../utils/util.js");
Page({

  /**
   * 页面的初始数据
   */
  data: {
    scrollHeight: 0,
    dataList: [],
    perType: wx.getStorageSync('LOGINDATA').perType,
    page: 1,
    total: 0,
    projectId: '',
    curdate: util.nowTime('date'),
    type: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let windowHeight = wx.getSystemInfoSync().windowHeight // 屏幕的高度
    console.log(options)
    this.setData({
      scrollHeight: windowHeight,
      projectId: options.projectId,
      type: options.type
    })
    this.getData()
  },
  getData() {
    // 获取页面初始数据
    let that = this
    let data = {}
    if (that.data.type == 3) {
      data = {
        ctcId: that.data.projectId,
        curdate: that.data.curdate,
        page: that.data.page,
        limit: 10
      }
    } else if (that.data.type == 1) {
      data = {
        ctcId: that.data.projectId,
        page: that.data.page,
        limit: 10
      }
    }
    call.httpService("/daily/list", data, "post", (res) => {
        if (res.code === 0) {
          if (that.data.perType == 1 && that.data.type == 3) {
            let url = res.data.length == 1 ? ('/pages/dayReport/dayReport?dailyId=' + res.data[0].dailyId + '&projectId=' + that.data.projectId) : '/pages/dayReport/dayReport?projectId=' + that.data.projectId
            wx.navigateTo({
              url: url
            })
          }
          that.setData({
            dataList: that.page == 1 ? res.data : that.data.dataList.concat(res.data),
            total: res.count
          });


        } else {
          wx.showToast({
            title: res.msg,
            icon: "none",
          });
        }
        wx.hideLoading();
      },
      (err) => {
        console.log(err)
        wx.hideLoading();
      }
    );
  },
  scrolltolower() {
    if (this.data.total > this.data.dataList.length) {
      this.setData({
        page: this.data.page + 1
      })
      this.getData()
    }
  },
  toDayReport(e) {
    // 跳转日报详情
    wx.navigateTo({
      url: '/pages/dayReport/dayReport?dailyId=' + e.currentTarget.dataset.id + '&projectId=' + this.data.projectId+'&reportDate=' + e.currentTarget.dataset.date.slice(0,10)
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})