// pages/tabs/index.js
const call = require("../../utils/request.js");

Page({

    /**
     * 页面的初始数据
     */
    data: {
        indicatorDots: false,
        autoplay: true,
        interval: 4000,
        duration: 500,
        carouselList: [],
        caseList: [],
        scrollHeight: 0,
        host: call.hostUrl,
        tabs: [
            {
                tab: '所有',
                tabId: '0'
            },
            {
                tab: '别墅',
                tabId: '1'
            },
            {
                tab: '大平层',
                tabId: '2'
            },
            {
                tab: '刚需家装',
                tabId: '3'
            },
            {
                tab: '工装',
                tabId: '4'
            }
        ],
        activeTab: 0
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        let windowHeight = wx.getSystemInfoSync().windowHeight // 屏幕的高度
        this.setData({
            scrollHeight: windowHeight - 199
        })
    },
    // 获取页面数据
    getData() {
        call.httpService("/caselist", "", "get", (res) => {
                if (res.code === 0) {
                    this.setData({
                        carouselList: res.data.carousellist,
                        caseOrigin:JSON.parse(JSON.stringify(res.data.caselist)),
                        caseList: res.data.caselist
                    })

                } else {
                    wx.showToast({
                        title: res.msg,
                        icon: "none",
                    });
                }
                wx.hideLoading();
            },
            (err) => {
                console.log(err)
                wx.hideLoading();
            }, true
        );
    },
    // 跳转案例详情
    toDetail(e) {
        // console.log(e.currentTarget.dataset.id)
        wx.navigateTo({
            url: '/pages/home/caseDetail?id=' + e.currentTarget.dataset.id,
        })
    },
    handleTabClick(e) {
        let index = e.currentTarget.dataset.index
        let data = []
        if(index){
            this.data.caseOrigin.map((item)=>{
                if(this.data.tabs[index].tabId == item.tags){
                    data.push(item)
                }
            })
        }else{
            data = JSON.parse(JSON.stringify(this.data.caseOrigin))
        }
        
        this.setData({
            activeTab: index,
            caseList:data
        })
    },
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {
        this.getData()
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})