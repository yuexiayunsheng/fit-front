// pages/home/caseDetail.jsconst 
const call = require("../../utils/request.js");
Page({

  /**
   * 页面的初始数据
   */
  data: {
    caseData:{
      articleDesc:'',
      articleId:'',
      caseDesc:'',
      imgUrl:'',
      title:'',
    },
    caseId:''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options)
    let that = this
    

    if (options.id) {
      that.setData({
        caseId: options.id
      })
      wx.showLoading({
        title: '加载中...',
      })
      this.getData()
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },
  // 获取案例详情
  getData() {
    let data={
      id: this.data.caseId
    }
    call.httpService("/casebyid", data, "get", (res) => {
      if (res.code === 0) {
        this.setData({
          articleDesc:res.data.articleDesc,
          articleId:res.data.articleId,
          caseDesc:res.data.caseDesc,
          imgUrl:res.data.imgUrl,
          title:res.data.title
        });
        wx.setNavigationBarTitle({
          title: res.data.title
        })
      } else {
        wx.showToast({
          title: res.msg,
          icon: "none",
        });
      }
      wx.hideLoading();
    },
      (err) => {
        wx.hideLoading();
      }
    );
  },
  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})